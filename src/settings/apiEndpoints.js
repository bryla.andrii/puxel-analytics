export default {
    hourly: 'https://analytics-api-pixel.clipr.co/impressions/hourly',
    daily: 'https://analytics-api-pixel.clipr.co/impressions/daily',
    total: 'https://analytics-api-pixel.clipr.co/impressions/total',
}
