import { defineStore } from 'pinia'
import { useHourlyStore } from './modules/hourly'
import { useDailyStore } from './modules/daily'
import { useTotalStore } from './modules/total'
import { useGeneralStore } from './modules/general'
import { ref } from 'vue'

export const useAnalyticsStore = defineStore('analytics', () => {
    const generalStore = useGeneralStore()
    const hourlyStore = useHourlyStore()
    const dailyStore = useDailyStore()
    const totalStore = useTotalStore()
    const dateRange = ref({})

    function loadAnalyticsData({ startDate, endDate }) {
        if (dateRange.value.startDate !== startDate || dateRange.value.endDate !== endDate) {
            dateRange.value = { startDate, endDate }
            return Promise.all([
                hourlyStore.hourlyLoadList(startDate, endDate),
                dailyStore.dailyLoadList(startDate, endDate),
                totalStore.totalLoadList(startDate, endDate),
            ])
        }
    }
    return {
        loadAnalyticsData,
        ...generalStore,
        ...hourlyStore,
        ...dailyStore,
        ...totalStore,
    }
})
