import apiEndpoints from '@/settings/apiEndpoints'
import axios from 'axios'
import { ref } from 'vue'
import { useGeneralStore } from '../modules/general'
import { getDashedDate } from '@/utils/utils'

export const getStoreTemplate = (storeLabel) => {
    const itemsList = ref([])
    const generalStore = useGeneralStore()
    async function loadItems(startDate, endDate) {
        itemsList.value = await generalStore.loaderWrapper(() =>
            axios
                .get(apiEndpoints[storeLabel], {
                    params: {
                        startDate: getDashedDate(startDate),
                        endDate: getDashedDate(endDate),
                    },
                })
                .then((res) => res.data)
        )
    }

    return {
        [`${storeLabel}List`]: itemsList,
        [`${storeLabel}LoadList`]: loadItems,
    }
}
