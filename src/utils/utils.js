export function formatBigNumbers(val) {
    return val.toString().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,')
}

export function getDashedDate(date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}
