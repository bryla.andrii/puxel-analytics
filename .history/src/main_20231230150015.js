import { createApp } from 'vue'
import App from './App.vue'
import Oruga from '@oruga-ui/oruga-next'
import { createPinia } from 'pinia'
import { useAnalyticsStore } from './store/analytics'
const pinia = createPinia()
createApp(App).use(pinia).use(Oruga).mount('#app')
const analyticsStore = useAnalyticsStore()
