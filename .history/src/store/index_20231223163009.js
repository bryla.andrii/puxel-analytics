import { defineStore } from 'pinia'

export const useAnalyticsStore = defineStore('alerts', {
    state: () => ({
        count: 0,
    }),
    getters: {
        doubleCount: (state) => state.count * 2,
    },
    actions: {
        // оскільки ми покладаємося на `this`, ми не можемо використовувати стрілочну функцію
        increment() {
            this.count++
        },
        randomizeCounter() {
            this.count = Math.round(100 * Math.random())
        },
    },
})
