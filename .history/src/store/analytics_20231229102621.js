import { defineStore } from 'pinia'
import { useHourlyStore } from './modules/hourly'
import { useDailyStore } from './modules/daily'
import { useTotalStore } from './modules/total'
import { useGeneralStore } from './modules/general'

export const useAnalyticsStore = defineStore('analytics', () => {
    const hourlyStore = useHourlyStore()
    const dailyStore = useDailyStore()
    const totalStore = useTotalStore()
    const generalStore = useGeneralStore()

    function loadAnalyticsData(startDate, endDate) {
        return Promise.all([
            hourlyStore.hourlyLoadList(startDate, endDate),
            dailyStore.dailyLoadList(startDate, endDate),
            totalStore.totalLoadList(startDate, endDate),
        ])
    }
    return {
        loadAnalyticsData,
        ...generalStore,
        ...hourlyStore,
        ...dailyStore,
        ...totalStore,
    }
})
