import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useGeneralStore = defineStore('general', () => {
    const loading = ref(0)
    const error = ref(null)

    function startLoading() {
        loading.value++
        error.value = null
    }
    function loaderWrapper(callback) {
        startLoading()
        return new Promise((resolve) => {
            callback()
                .then((res) => resolve(res))
                .catch((resError) => (error.value = resError))
                .finally(() => {
                    loading.value--
                })
        })
    }
    return {
        loading,
        error,
        loaderWrapper,
    }
})
