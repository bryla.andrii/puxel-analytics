import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
import { useGeneralStore } from './general'
export const useTotalStore = defineStore('total', () => {
    const generalStore = useGeneralStore()
    return getStoreTemplate('total', generalStore)
})
