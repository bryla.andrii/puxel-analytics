import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
export const useDailyStore = defineStore('daily', () => {
    return getStoreTemplate('daily')
})
