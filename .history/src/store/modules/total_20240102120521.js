import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
export const useTotalStore = defineStore('total', () => {
    return getStoreTemplate('total')
})
