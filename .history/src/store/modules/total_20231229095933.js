import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
import { useGeneralStore } from './general'
export const useHourlyStore = defineStore('hourly', () => {
    const generalStore = useGeneralStore()
    return getStoreTemplate('hourly', generalStore)
})
