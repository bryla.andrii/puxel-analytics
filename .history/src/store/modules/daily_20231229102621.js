import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
import { useGeneralStore } from './general'
export const useDailyStore = defineStore('daily', () => {
    const generalStore = useGeneralStore()
    return getStoreTemplate('daily', generalStore)
})
