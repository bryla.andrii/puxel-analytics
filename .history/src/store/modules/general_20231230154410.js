import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useGeneralStore = defineStore('general', () => {
    const loading = ref(0)
    const error = ref(null)

    function startLoading() {
        loading.value++
        error.value = null
        console.log('====loading.value')
        console.log(loading.value)
    }
    function loaderWrapper(callback) {
        startLoading()
        return new Promise((resolve) => {
            try {
                let res = callback()
                resolve(res)
            } catch (resError) {
                error.value = resError
                // reject(resError)
            } finally {
                loading.value--
                console.log('loading.value--')
                console.log(loading.value)
            }
        })
    }
    return {
        loading,
        error,
        loaderWrapper,
    }
})
