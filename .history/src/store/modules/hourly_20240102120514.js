import { defineStore } from 'pinia'
import { getStoreTemplate } from '../helpers/storeTemplate'
export const useHourlyStore = defineStore('hourly', () => {
    return getStoreTemplate('hourly')
})
