import apiEndpoints from '@/settings/apiEndpoints'
import axios from 'axios'
import { ref } from 'vue'
import { useGeneralStore } from '../modules/general'

export const getStoreTemplate = (storeLabel) => {
    const itemsList = ref([])
    const generalStore = useGeneralStore()
    // async function loadItems(startDate, endDate) {
    async function loadItems() {
        itemsList.value = await generalStore.loaderWrapper(() =>
            axios
                .get(apiEndpoints[storeLabel], {
                    params: {
                        // startDate: startDate.toISOString().split('T')[0],
                        // endDate: endDate.toISOString().split('T')[0],
                        startDate: '2023-11-25',
                        endDate: '2023-12-20',
                    },
                })
                .then((res) => res.data)
        )
    }

    return {
        [`${storeLabel}List`]: itemsList,
        [`${storeLabel}LoadList`]: loadItems,
    }
}
