export const getStoreTemplate = (storeLabel) => ({
    state: () => ({
        [`${storeLabel}List`]: [],
    }),
    getters: {
        doubleCount: (state) => state.count * 2,
    },
    actions: {
        increment() {
            this.count++
        },
        randomizeCounter() {
            this.count = Math.round(100 * Math.random())
        },
    },
})
