import { defineStore } from 'pinia'

export const useGeneralStore = defineStore('general', {
    state: () => ({
        loading: 0,
        error: null,
    }),
    getters: {
        isLoading: (state) => state.loading,
        hasError: (state) => state.error,
    },
    actions: {
        startLoading() {
            this.loading++
            this.error = null
        },
        async actionMethod(callback) {
            this.startLoading()
            return new Promise((resolve, reject) => {
                try {
                    let res = callback()
                    resolve(res)
                } catch (error) {
                    reject(error)
                } finally {
                    this.loading--
                }
            })
        },
    },
})
