import apiEndpoints from '@/settings/apiEndpoints'
import axios from 'axios'
export const getStoreTemplate = (storeLabel, generalStore) => ({
    state: () => ({
        [`${storeLabel}List`]: [],
    }),
    // getters: {
    //     doubleCount: (state) => state.count * 2,
    // },
    actions: {
        async [`load${storeLabel}List`](startDate, endDate) {
            this[`${storeLabel}List`] = await generalStore.loaderWrapper(
                axios.get(apiEndpoints[storeLabel], {
                    params: {
                        startDate,
                        endDate,
                    },
                })
            )
        },
    },
})
