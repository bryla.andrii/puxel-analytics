import apiEndpoints from '@/settings/apiEndpoints'
import axios from 'axios'
import { ref } from 'vue'
export const getStoreTemplate = (storeLabel, generalStore) => {
    const itemsList = ref([])

    async function loadItems(startDate, endDate) {
        itemsList.value = await generalStore.loaderWrapper(
            async () =>
                await axios.get(apiEndpoints[storeLabel], {
                    params: {
                        startDate,
                        endDate,
                    },
                })
        )
    }

    return {
        [`${storeLabel}List`]: itemsList,
        [`${storeLabel}LoadList`]: loadItems,
    }
}
