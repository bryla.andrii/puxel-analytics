import apiEndpoints from '@/settings/apiEndpoints'
import axios from 'axios'
export const getStoreTemplate = (storeLabel, generalStore) => {
    const itemsList = []

    async function loadItems(startDate, endDate) {
        this[`${storeLabel}List`] = await generalStore.loaderWrapper(
            axios.get(apiEndpoints[storeLabel], {
                params: {
                    startDate,
                    endDate,
                },
            })
        )
    }

    return {
        [`${storeLabel}List`]: itemsList,
        [`load${storeLabel}List`]: loadItems,
    }
}
