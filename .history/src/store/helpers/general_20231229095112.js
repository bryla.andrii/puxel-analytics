import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useGeneralStore = defineStore('general', ()=> {
    const loading = ref (0)
    const error=ref(null)


    function startLoading() {
            this.loading++
            this.error = null
        }
        function loaderWrapper(callback) {
            this.startLoading()
            return new Promise((resolve, reject) => {
                try {
                    let res = callback()
                    resolve(res)
                } catch (error) {
                    reject(error)
                } finally {
                    this.loading--
                }
            })
        }
    }
})
