import { createApp } from 'vue'
import App from './App.vue'
import Oruga from '@oruga-ui/oruga-next'
import { createPinia } from 'pinia'

createApp(App).use(createPinia()).use(Oruga).mount('#app')
