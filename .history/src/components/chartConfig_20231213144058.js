export const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
        {
            label: 'Data One',
            backgroundColor: '#f87979',
            data: [40, 39, 10, 40, 39, 80, 40],
            tension: 0.3,
        },
    ],
}

export const options = {
    responsive: true,
    maintainAspectRatio: false,
}
