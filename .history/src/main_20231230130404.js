import { createApp } from 'vue'
import App from './App.vue'
import Oruga from '@oruga-ui/oruga-next'
import { createPinia } from 'pinia'
const pinia = createPinia()
createApp(App).use(pinia).use(Oruga).mount('#app')
