export function formatBigNumbers(val) {
    return val.toString().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,')
}
