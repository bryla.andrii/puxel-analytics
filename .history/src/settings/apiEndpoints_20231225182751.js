export default {
    hourly: 'https://analytics-api-pixel.clipr.co/impressions/hourly',
    daily: (startDate, endDate) =>
        `https://analytics-api-pixel.clipr.co/impressions/daily?startDate=${startDate}&endDate=${endDate}`,
    total: (startDate, endDate) =>
        `https://analytics-api-pixel.clipr.co/impressions/total?startDate=${startDate}&endDate=${endDate}`,
}

// https://analytics-api-pixel.clipr.co/impressions/hourly?startDate=2023-11-25&endDate=2023-12-25
// https://analytics-api-pixel.clipr.co/impressions/daily?startDate=2023-11-25&endDate=2023-12-25
// https://analytics-api-pixel.clipr.co/impressions/total?startDate=2023-11-25&endDate=2023-12-25
